# Software Evolution and Program Comprehension - Practical Assignment
For the practical assignment of CES, the chosen project to contribute
was neomutt. As requested, this report is comprised of three main parts.
An initial description of the application, a set of reports on the issues
to be fixed and a final Project Maintainability Status.

Each section is self-contained in its own file and the links are displayed
below.

- [Description of the Project](src/initial_description.md)
- Issues Description:
	- [First Issue](src/issue-1.md)
	- [Second Issue](src/issue-2.md)
- [Project Maintainability Status](src/maintainability_report.md)
