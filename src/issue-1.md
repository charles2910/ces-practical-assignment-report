# First Issue - Move Browser Config Variables to Browser Library
## Issue Caractherization
Neomutt is a fork of mutt. It maintains full compatibility and feature set,
but aims to improve modularity, design and maintainability. Since its codebase
was initially shared with mutt's, some things remain the same until there is
a opportunity to change.

In mutt, all configuration variables reside in a single file - a 5k lines
one. Accordingly to its goals, neomutt tries to modularize the code using
the library concept. Every congruent and closely related (in the feature
sense) set of files, code, configs is grouped together as a library e.g.
index, imap, sidebar, etc.

[Issue #181](https://github.com/neomutt/neomutt/issues/181) is a feature
request related to the browser library. And it proved to be an excellent
oportunity to change old code.

There were some config variable definitions related to the browser lib
still in the mutt reminiscent place. So, one thing requested prior to
doing #181 was relocating these config variables to the browser lib.

## Requirements
- Feature: Modularity
	- The code should be modular in order to reduce coupling, separate
	responsabilities and encapsulate implementation details.

## Source Code Files
As mentioned previously, neomutt codebase is comprised of several modules.
Each of them has a set of responsabilities and provide a set of features.
This task is related to the `browser` lib, we want to move code closely
related to it into (additions) the module (in this case, the browser folder
in the source code). Now, there are two special places from where we'd like
to take code out. The first is the `mutt_config.c` file (containing every
config that has not yet been moved to a module) and the other is the nntp
module (this has to do with config that is more related to browser, but
was placed in this lib previously).

## Designing the Fix
The steps to make this change can be broken down in:
1. Create a new config file for the browser library.
	- In order to move these configs to the new library, we must create a new
	file for them under the `browser` folder (`browser/config.c`).

2. Move the config vars to the new file.
	- It's basically just move the config vars declarations to a new vector of
	config variables.

3. Create and use a new function to register this new vector.
	- No good C code can get away without using C Macros, so here we create the
	`config_init_browser` function and use it with a macro in the `mutt_config.c`
	file.

4. Inform the building program of the new file.
	- We have to declare our new C file (`config.c`) in the `Makefile.autosetup`
	to let the build tool know it has to track and build this new file.

## The Fix
The fix was merged in neomutt in commit [b461f11](https://github.com/neomutt/neomutt/commit/b461f11bcf84c7c5d116429e67a1ec04cdfb5749)
and it is displayed below to indicate the design steps in practice.

- Step 4:

```diff
diff --git a/Makefile.autosetup b/Makefile.autosetup
index 47ea99d66..6f72a8903 100644
--- a/Makefile.autosetup
+++ b/Makefile.autosetup
@@ -166,7 +166,7 @@ $(PWD)/bcache:
 ###############################################################################
 # libbrowser
 LIBBROWSER=    libbrowser.a
-LIBBROWSEROBJS=        browser/browser.o browser/sort.o
+LIBBROWSEROBJS=        browser/browser.o browser/config.o browser/sort.o
 CLEANFILES+=   $(LIBBROWSER) $(LIBBROWSEROBJS)
 ALLOBJS+=      $(LIBBROWSEROBJS)

```

- Steps 1, 2 and 3

```diff
diff --git a/browser/config.c b/browser/config.c
new file mode 100644
index 000000000..c8484de21
--- /dev/null
+++ b/browser/config.c
@@ -0,0 +1,65 @@
+/**
+ * @file
+ * Config used by libbrowser
+ *
+ * @authors
+ * Copyright (C) 2021 Richard Russon <rich@flatcap.org>
+ * Copyright (C) 2022 Carlos Henrique Lima Melara <charlesmelara@outlook.com>
+ *
+ * @copyright
+ * This program is free software: you can redistribute it and/or modify it under
+ * the terms of the GNU General Public License as published by the Free Software
+ * Foundation, either version 2 of the License, or (at your option) any later
+ * version.
+ *
+ * This program is distributed in the hope that it will be useful, but WITHOUT
+ * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
+ * details.
+ *
+ * You should have received a copy of the GNU General Public License along with
+ * this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+/**
+ * @page browser_config Config used by libbrowser
+ *
+ * Config used by libbrowser
+ */
+
+#include "config.h"
+#include <stddef.h>
+#include <config/lib.h>
+#include <stdbool.h>
+
+static struct ConfigDef BrowserVars[] = {
+  // clang-format off
+  { "browser_abbreviate_mailboxes", DT_BOOL, true, 0, NULL,
+    "Abbreviate mailboxes using '~' and '=' in the browser"
+  },
+  { "folder_format", DT_STRING|DT_NOT_EMPTY|R_MENU, IP "%2C %t %N %F %2l %-8.8u %-8.8g %8s %d %i", 0, NULL,
+    "printf-like format string for the browser's display of folders"
+  },
+  { "group_index_format", DT_STRING|DT_NOT_EMPTY|R_INDEX|R_PAGER, IP "%4C %M%N %5s  %-45.45f %d", 0, NULL,
+    "(nntp) printf-like format string for the browser's display of newsgroups"
+  },
+  { "mask", DT_REGEX|DT_REGEX_MATCH_CASE|DT_REGEX_ALLOW_NOT|DT_REGEX_NOSUB, IP "!^\\.[^.]", 0, NULL,
+    "Only display files/dirs matching this regex in the browser"
+  },
+  { "show_only_unread", DT_BOOL, false, 0, NULL,
+    "(nntp) Only show subscribed newsgroups with unread articles"
+  },
+  { "sort_browser", DT_SORT|DT_SORT_REVERSE, SORT_ALPHA, IP SortBrowserMethods, NULL,
+    "Sort method for the browser"
+  },
+  { NULL },
+  // clang-format on
+};
+
+/**
+ * config_init_browser - Register browser config variables - Implements ::module_init_config_t - @ingroup cfg_module_api
+ */
+bool config_init_browser(struct ConfigSet *cs)
+{
+  return cs_register_variables(cs, BrowserVars, 0);
+}
diff --git a/browser/lib.h b/browser/lib.h
index 8a841fddd..1fcdbd0d0 100644
--- a/browser/lib.h
+++ b/browser/lib.h
@@ -28,6 +28,7 @@
  * | File                | Description                |
  * | :------------------ | :------------------------- |
  * | browser/browser.c   | @subpage browser_browser   |
+ * | browser/config.c    | @subpage browser_config    |
  * | browser/sort.c      | @subpage browser_sorting   |
  */
 
diff --git a/mutt_config.c b/mutt_config.c
index 49c4749d1..1ffbff77c 100644
--- a/mutt_config.c
+++ b/mutt_config.c
@@ -209,9 +209,6 @@ static struct ConfigDef MainVars[] = {
   { "braille_friendly", DT_BOOL, false, 0, NULL,
     "Move the cursor to the beginning of the line"
   },
-  { "browser_abbreviate_mailboxes", DT_BOOL, true, 0, NULL,
-    "Abbreviate mailboxes using '~' and '=' in the browser"
-  },
   { "charset", DT_STRING|DT_NOT_EMPTY|DT_CHARSET_SINGLE, 0, 0, charset_validator,
     "Default character set for displaying text on screen"
   },
@@ -278,9 +275,6 @@ static struct ConfigDef MainVars[] = {
   { "folder", DT_STRING|DT_MAILBOX, IP "~/Mail", 0, NULL,
     "Base folder for a set of mailboxes"
   },
-  { "folder_format", DT_STRING|DT_NOT_EMPTY|R_MENU, IP "%2C %t %N %F %2l %-8.8u %-8.8g %8s %d %i", 0, NULL,
-    "printf-like format string for the browser's display of folders"
-  },
   { "force_name", DT_BOOL, false, 0, NULL,
     "Save outgoing mail in a folder of their name"
   },
@@ -377,9 +371,6 @@ static struct ConfigDef MainVars[] = {
   { "markers", DT_BOOL|R_PAGER_FLOW, true, 0, NULL,
     "Display a '+' at the beginning of wrapped lines in the pager"
   },
-  { "mask", DT_REGEX|DT_REGEX_MATCH_CASE|DT_REGEX_ALLOW_NOT|DT_REGEX_NOSUB, IP "!^\\.[^.]", 0, NULL,
-    "Only display files/dirs matching this regex in the browser"
-  },
   { "mbox", DT_STRING|DT_MAILBOX|R_INDEX|R_PAGER, IP "~/mbox", 0, NULL,
     "Folder that receives read emails (see Move)"
   },
@@ -557,9 +548,6 @@ static struct ConfigDef MainVars[] = {
   { "sort_aux", DT_SORT|DT_SORT_REVERSE|DT_SORT_LAST|R_INDEX|R_RESORT|R_RESORT_SUB, SORT_DATE, IP SortAuxMethods, NULL,
     "Secondary sort method for the index"
   },
-  { "sort_browser", DT_SORT|DT_SORT_REVERSE, SORT_ALPHA, IP SortBrowserMethods, NULL,
-    "Sort method for the browser"
-  },
   { "sort_re", DT_BOOL|R_INDEX|R_RESORT|R_RESORT_INIT, true, 0, NULL,
     "Whether $reply_regex must be matched when not $strict_threads"
   },
@@ -757,6 +745,7 @@ static void init_variables(struct ConfigSet *cs)
 #if defined(USE_AUTOCRYPT)
   CONFIG_INIT_VARS(cs, autocrypt);
 #endif
+  CONFIG_INIT_VARS(cs, browser);
   CONFIG_INIT_VARS(cs, compose);
   CONFIG_INIT_VARS(cs, conn);
 #if defined(USE_HCACHE)
diff --git a/nntp/config.c b/nntp/config.c
index cbad33c3c..8ed6d13df 100644
--- a/nntp/config.c
+++ b/nntp/config.c
@@ -39,9 +39,6 @@ static struct ConfigDef NntpVars[] = {
   { "followup_to_poster", DT_QUAD, MUTT_ASKYES, 0, NULL,
     "(nntp) Reply to the poster if 'poster' is in the 'Followup-To' header"
   },
-  { "group_index_format", DT_STRING|DT_NOT_EMPTY|R_INDEX|R_PAGER, IP "%4C %M%N %5s  %-45.45f %d", 0, NULL,
-    "(nntp) printf-like format string for the browser's display of newsgroups"
-  },
   { "newsgroups_charset", DT_STRING, IP "utf-8", 0, charset_validator,
     "(nntp) Character set of newsgroups' descriptions"
   },
@@ -84,9 +81,6 @@ static struct ConfigDef NntpVars[] = {
   { "show_new_news", DT_BOOL, true, 0, NULL,
     "(nntp) Check for new newsgroups when entering the browser"
   },
-  { "show_only_unread", DT_BOOL, false, 0, NULL,
-    "(nntp) Only show subscribed newsgroups with unread articles"
-  },
   { "x_comment_to", DT_BOOL, false, 0, NULL,
     "(nntp) Add 'X-Comment-To' header that contains article author"
   },
```

## Submiting the Fix
The fix was merged upstream without a PR. The main developer applied the commit
directly to the main project tree (the commit is listed above).
