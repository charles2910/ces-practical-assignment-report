# Project Maintainability Status - Neomutt

## Introduction
As the conclusion of the work done in this project, a brief report on
the maintainability of the software (neomutt) will be given.

A 6-scale grade system will be used to rank each category, **A** being
the best and **F** the worst grade. Not only the grade will be given,
but also a few appointments on how to improve the current status.


## Documentation
Neomutt has a very extensive manual for end users - it's called The
Manual. But most code documentation is stored in the code itself. On
one hand it's easier for developers to maintain it and keep up to date.
On the other hand, it's a bit more complicated for newer devs that aren't
so familiar with the code. For the latter, usually graphical representation
like diagrams tend to make it easier to understand the code. Although
neomutt has some dependency graphs in the documentation (which functions
are called within and who are the callers of the function), it would be
very nice to have other diagrams documenting the sequencing of interactions
and main components of the application.

- **Grade: B**

## Source Code
Neomutt is written in C and this has some implications. It's a more
simple language than newer ones (like python and javascript). This
is nice because it's easier to remember the syntax, but also has
limitations on the library side - it's not easy to import libs for
everything and just focus on the application logic. So most of
dependencies are also written and maintained together with the
main application.

Although it's a very extensive codebase, the modular approach
chosen by the developers makes it easier to understand. Also,
changes are mostly self-contained to a file - or a couple files -
within a module. This proved to be a very valuable characteristic
when tackling the issues.

Most functions, aside from the main function of each module, tend
to be small and easy to comprehend. They come with a description
showing the parameters taken and giving a short summary of their
purpose.

As a final note, let's remember that neomutt is a fork of mutt
and it's main goal is to have a more organised, maintainable and
approachable source code than mutt. In my opinion the goal has
been achieved.

- **Grade: A**

## Community
The neomutt community is awesome. They are very welcoming and
helpfull to newcommers. All questions asked were answered and they
provided a thorough explanation on source code related to the change
proposed. As all voluntary based open source work, sometimes it takes
a while for the changes to be reviewed by the comunity, but it was
faster than the average with neomutt developers. All in all, it was a
very nice experience and I'll keep contributing to it.

- **Grade: A**
