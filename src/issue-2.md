# Second Issue - Add mailbox_folder_format Config Variable
## Issue Caractherization
[Issue #181](https://github.com/neomutt/neomutt/issues/181) is a feature
request for neomutt which affects the `browser` library. This library is
responsible for displaying folders listing (similar to `ls`). It's commonly
used when **browser**ing the file system to select an attachment or a mailbox
in the MailDir format. Nevertheless neomutt can access and display remote
mailboxes through specific protocols (e.g. imap, pop3). They also allow
folder nesting and the user must be able to change folders. In this case,
the browser is also used, but most of the information regarding local files
isn't present. So the browser for these remote use cases tend to have less
useful information. The first picture below depicts a file browsering to
select an attachment (similar to `ls`), the second display an imap folder
selection (useless info).

![File selection](/img/files.png)
![Imap folder selection](/img/useless.png)

This is the motivation of the feature request, let's now talk about the
feature requested. The issue author asks for a new variable that specifies
a browser display format specific for the remote mailboxes so it can display
more useful information to the users.

## Requirements
- As an user, I want to see relevant information about my remote mailboxes.
- As an user, I want to customize what's displayed in the browser for remote
mailboxes.

## Source Code Files
As this is a feature implementation in the `browser` library, the code change
is mostly self-contained to the library itself. The only exception is in
documentation. Since Neomutt has a dedicated folder for documentation, we
must document this feature in there.

The source files of interest in the `browser` lib are `browser.c` which contains
a good part of the main library code and `config.c` which contain the config
variables. On the documentation side (`docs` folder), the file of interest is
`config.c` which contains the manual documentation for every config variable
available in neomutt.

## Designing the Fix
Here, we have to tackle two main points to meet our goal. Namely:
1. Create the new config variable;
2. Make use of it the the code.

We have already an ideia on how to do this first part because of the previous
issue, but let's recapitulate. The config variables of a library are declared
in the `config.c` file within the lib - in our case `/browser/config.c`. In
there, we have a vector `BrowserVars` that contains the variables. The
variables are actually a struct `ConfigDef` which has the following fields:
- Name
- Type/Flags
- Variable
- Initial Value (default)
- Data
- Validator function
- Short description

We basically have to add a new variable called `mailbox_folder_format` to this
vector.

The second part is a bit more challenging because it requires knowledge
about the library. Thankfully, neomutt has wonderful mentors that helped
here. The library has a function that is responsible for formating each
entry of the list (the list can be comprised of files, folders, etc) -
it's `folder_make_entry()`. This fucntion calls another one to expand
the line accordingly to the `folder_format_string`.

If we want to use another var to describe the format, we have to change
this `folder_make_entry` to use our `mailbox_folder_format` variable. The
only caveat is we only want that for remote mailboxes, so we need to be
able to access this info inside the function. One of the arguments passed
to it is the `menu` which is a `Menu` struct which defines the content of
the screen. It has a `mdata` field used to store relevant data. What it
stores is defined in `mutt_buffer_select_file()` which is the main function
of the lib, currently it stores the `state.entry` in the `mdata` field,
which are the entries to be displayed in the browser.

If we look closer to the `state` field, we'll discover it is a `BrowserState`
struct and it contains a field `is_mailbox_list` that is set only when
displaying a non-local mailbox. If we can access this field in the
`folder_make_entry` function, our work will be easy.

## The Fix
For the first part, we have to include the config variable and document it:

```diff
diff --git a/browser/config.c b/browser/config.c
index 097d3077e..f7efee359 100644
--- a/browser/config.c
+++ b/browser/config.c
@@ -44,6 +44,9 @@ static struct ConfigDef BrowserVars[] = {
   { "group_index_format", DT_STRING|DT_NOT_EMPTY|R_INDEX|R_PAGER, IP "%4C %M%N %5s  %-45.45f %d", 0, NULL,
     "(nntp) printf-like format string for the browser's display of newsgroups"
   },
+  { "mailbox_folder_format", DT_STRING|DT_NOT_EMPTY|R_MENU, IP "%2C %t %N %6n %6m %i", 0, NULL,
+    "printf-like format string for the browser's display of mailbox folders"
+  },
   { "mask", DT_REGEX|DT_REGEX_MATCH_CASE|DT_REGEX_ALLOW_NOT|DT_REGEX_NOSUB, IP "!^\\.[^.]", 0, NULL,
     "Only display files/dirs matching this regex in the browser"
   },
diff --git a/docs/config.c b/docs/config.c
index 616c1c16e..3fed0ef85 100644
--- a/docs/config.c
+++ b/docs/config.c
@@ -2210,6 +2210,43 @@
 ** how often (in seconds) NeoMutt will update message counts.
 */
 
+{ "mailbox_folder_format", DT_STRING, "%2C %t %N %n %m %i" },
+/*
+** .pp
+** This variable allows you to customize the file browser display to your
+** personal taste. It's only used to customize network mailboxes (e.g. imap).
+** This string is similar to $$index_format, but has its own set of
+** \fCprintf(3)\fP-like sequences:
+** .dl
+** .dt %C  .dd   .dd Current file number
+** .dt %d  .dd   .dd Date/time folder was last modified
+** .dt %D  .dd   .dd Date/time folder was last modified using $$date_format.
+** .dt %f  .dd   .dd Filename ("/" is appended to directory names,
+**                   "@" to symbolic links and "*" to executable files)
+** .dt %F  .dd   .dd File permissions
+** .dt %g  .dd   .dd Group name (or numeric gid, if missing)
+** .dt %i  .dd   .dd Description of the folder
+** .dt %l  .dd   .dd Number of hard links
+** .dt %m  .dd * .dd Number of messages in the mailbox
+** .dt %n  .dd * .dd Number of unread messages in the mailbox
+** .dt %N  .dd   .dd "N" if mailbox has new mail, blank otherwise
+** .dt %s  .dd   .dd Size in bytes (see $formatstrings-size)
+** .dt %t  .dd   .dd "*" if the file is tagged, blank otherwise
+** .dt %u  .dd   .dd Owner name (or numeric uid, if missing)
+** .dt %>X .dd   .dd Right justify the rest of the string and pad with character "X"
+** .dt %|X .dd   .dd Pad to the end of the line with character "X"
+** .dt %*X .dd   .dd Soft-fill with character "X" as pad
+** .de
+** .pp
+** For an explanation of "soft-fill", see the $$index_format documentation.
+** .pp
+** * = can be optionally printed if nonzero
+** .pp
+** %m, %n, and %N only work for monitored mailboxes.
+** %m requires $$mail_check_stats to be set.
+** %n requires $$mail_check_stats to be set (except for IMAP mailboxes).
+*/
+
 { "mailcap_path", DT_SLIST, "~/.mailcap:" PKGDATADIR "/mailcap:" SYSCONFDIR "/mailcap:/etc/mailcap:/usr/etc/mailcap:/usr/local/etc/mailcap" },
 /*
 ** .pp
```

Now, for the second, we have to pass more info to the `folder_make_entry`
function through the `menu->mdata` field. So instead of storing `state.entry`
in it, we can store `state` (we are actually storing addresses, but I think
that is more clear not to mention it). Now, we just have to test if
`is_mailbox_list` is set and then - if it is - use our `mailbox_folder_format`
config variable:

```diff
diff --git a/browser/browser.c b/browser/browser.c
index 0ace858db..527fa7098 100644
--- a/browser/browser.c
+++ b/browser/browser.c
@@ -805,7 +805,8 @@ static int select_file_search(struct Menu *menu, regex_t *rx, int line)
  */
 static void folder_make_entry(struct Menu *menu, char *buf, size_t buflen, int line)
 {
-  struct BrowserStateEntry *entry = menu->mdata;
+  struct BrowserState *state = menu->mdata;
+  struct BrowserStateEntry *entry = &state->entry;
   struct Folder folder = {
     .ff = ARRAY_GET(entry, line),
     .num = line,
@@ -821,6 +822,14 @@ static void folder_make_entry(struct Menu *menu, char *buf, size_t buflen, int l
   }
   else
 #endif
+      if (state->is_mailbox_list)
+  {
+    const char *const c_mailbox_folder_format = cs_subset_string(NeoMutt->sub, "mailbox_folder_format");
+    mutt_expando_format(buf, buflen, 0, menu->win->state.cols,
+                        NONULL(c_mailbox_folder_format), folder_format_str,
+                        (intptr_t) &folder, MUTT_FORMAT_ARROWCURSOR);
+  }
+  else
   {
     const char *const c_folder_format = cs_subset_string(NeoMutt->sub, "folder_format");
     mutt_expando_format(buf, buflen, 0, menu->win->state.cols, NONULL(c_folder_format),
@@ -1313,7 +1322,7 @@ void mutt_buffer_select_file(struct Buffer *file, SelectFileFlags flags,
 
   init_menu(&priv->state, priv->menu, m, priv->sbar);
   // only now do we have a valid priv->state to attach
-  priv->menu->mdata = &priv->state.entry;
+  priv->menu->mdata = &priv->state;
 
   // ---------------------------------------------------------------------------
   // Event Loop
```

After the implemented fix, the browser for remote mailboxes looks like this:
![Remote mailboxes](/img/useful.png)

## Submiting the Fix
The fix has been submited as a PR in the github repository and is
currently waiting for a reviewer. It can be found in [#3412](https://github.com/neomutt/neomutt/pull/3412).
