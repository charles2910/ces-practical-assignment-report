# Initial Description - Neomutt
## What is Neomutt
Neomutt is a Mail Client, it can read mail from local mailboxes (in MailDir
and MailBox formats), from remote mailboxes (through IMAP or POP3) and allows
one to write and send emails too.

## How Alive is the Project
The project has weekly contributions from multiple contributors. The lead
developer (flatcap) is the most active one, but there is a community of
devs that review PRs.

In the past month, 10 contributors pushed commits to the repository changing
693 files. Also, in this timespan, one new release of neomutt was published.

Only this year, mroe than 400 commits were pushed to the repository. And,
if we extend it to january of 2021, we have more 1200 commits.

## How Important it is
It's a important project in two ways, it offers many functionalities to its
user and is a fork of mutt - a very popular mail client - with the intention
of being a more collaborative, approachable and modular than mutt.

## What is it Good for
It offers a lot of functionality, but one of the main advantages is the
flexibility and customisability neomutt offers. Users can take advantage
of custom hooks and keyboard shortcuts to tailor the application to one
specific needs.

## What are the Technologies Involved
Neomutt is mainly written in C, but involves many technologies and concepts
related to Mail, such as Mail formats, networking protocols for accessing
remote mailboxes, terminal rendering, etc.
